#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
DEPLOY="$THISDIR/deploy"
LOGS="$THISDIR/deploy-logs"

if [ "$USER" = 'root' ] ; then
    cat 1>&2 <<EOF
Only some scripts run by this program need elevated privileges. Run this
command as a suitable unprivileged user and prompting for additional privileges
will occur.
EOF
    exit 1
fi

if [ "$1" = '-n' ] ; then
    ECHO=echo
    shift 1
else
    ECHO=
fi

# Write script output to a dedicated directory.

if [ ! -e "$LOGS" ] ; then
    mkdir -p "$LOGS"
fi

for FILENAME in `find "$DEPLOY" -maxdepth 1 -type f | sort` ; do
    SCRIPT=`basename "$FILENAME"`

    # Run NN-root-* scripts as root.
    # Run NN-user-* scripts as the current user.

    if echo "$SCRIPT" | grep -q -e '^[[:digit:]]\+-root-' ; then
        PRIV=sudo
    elif echo "$SCRIPT" | grep -q -e '^[[:digit:]]\+-user-' ; then
        PRIV=
    else
        continue
    fi

    LOGFILE="$LOGS/$SCRIPT"

    if [ "$ECHO" ] ; then
        $ECHO $PRIV "$FILENAME"
    else
        echo "Executing script: $SCRIPT" 1>&2

        if ! $PRIV "$FILENAME" $* > "$LOGFILE" 2>&1 ; then
            echo "Script failed: $SCRIPT" 1>&2
            echo "See the log file for details: $LOGFILE" 1>&2
            exit 1
        fi
    fi
done
