#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR"`

if [ ! -e "$THISDIR/CSC" ] ; then
    git clone https://github.com/ge11232002/CSC "$THISDIR/CSC"
fi

cd "$THISDIR/CSC/JASPAR/PWMrandomization"
g++ -o "$BASEDIR/bin/PWMrandom" main.cc CLHEP/Random/*.cc

# vim: tabstop=4 expandtab shiftwidth=4
