JASPAR Tools
============

JASPAR_ is a database of curated, non-redundant set of profiles, derived from
published collections of experimentally defined transcription factor binding
sites for eukaryotes.

Several tools are provided via the JASPAR website, and this repository offers
a distribution of these tools.

.. _JASPAR: https://bitbucket.org/CBGR/jaspar2020/


Prerequisites
-------------

In order to obtain and deploy these JASPAR tools, certain other tools and
software packages are required:

======== ==================== ===============================================
Software Package              Details
======== ==================== ===============================================
``git``  ``git``              Used to obtain tool source code distributions
``g++``  ``gcc-c++``          Used to compile C++ programs
======== ==================== ===============================================

Here, the package names correspond to those used on Fedora or Red Hat systems.
In practice, a POSIX-compatible shell is also required.

To build documentation files (like this one), the Docutils and Pygments tools
are needed. On Fedora and Red Hat systems, the ``python3-docutils`` and
``python3-pygments`` packages provide such software. The reStructuredText_
format is used for documentation.

.. _reStructuredText: https://docutils.sourceforge.io/rst.html


Getting Started
---------------

First, get the development version of the software from Bitbucket using the
``git`` tool:

.. code-block:: shell

    git clone https://bitbucket.org/CBGR/jaspar_tools.git

For convenience, it is recommended that the above command be run so that the
``jaspar_tools`` repository is created alongside the JASPAR repository
(``jaspar2020``) in a common directory. Otherwise, additional configuration
may be required in JASPAR.

Now, enter the ``jaspar_tools`` directory and run the ``build.sh`` script to
obtain and compile the software:

.. code-block:: shell

    cd jaspar_tools
    ./build.sh

Alternatively, the ``deploy.sh`` script can be run to fetch packages and to
perform other system-related deployment tasks:

.. code-block:: shell

    cd jaspar_tools
    ./deploy.sh

Since this will require elevated privileges, it may not always be appropriate
or necessary.

Hopefully, the tools will now have been built, and the JASPAR documentation
can now be consulted with regard to configuring JASPAR to make use of these
tools.
