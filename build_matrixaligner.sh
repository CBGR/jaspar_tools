#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR"`

if [ ! -e "$THISDIR/matrixaligner" ] ; then

    # The sources may ultimately be available separately.

    cat 1>&2 <<EOF
The matrixaligner directory is missing from this distribution.

Please obtain an updated version of this distribution and run the updated
version of this script again.
EOF
    exit 1
fi

cd "$THISDIR/matrixaligner"
g++ -o "$BASEDIR/bin/matrix_aligner" matrix_aligner.cc

# vim: tabstop=4 expandtab shiftwidth=4
