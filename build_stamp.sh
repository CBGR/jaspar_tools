#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR"`

if [ ! -e "$THISDIR/stamp" ] ; then
    git clone https://github.com/seqcode/stamp "$THISDIR/stamp"
fi

cd "$THISDIR/stamp/src"
g++ -O3 -o "$BASEDIR/bin/stamp" Motif.cpp Alignment.cpp ColumnComp.cpp \
    PlatformSupport.cpp PlatformTesting.cpp Tree.cpp \
    NeuralTree.cpp MultipleAlignment.cpp RandPSSMGen.cpp \
    ProteinDomains.cpp main.cpp -lm -lgsl -lgslcblas

# vim: tabstop=4 expandtab shiftwidth=4
