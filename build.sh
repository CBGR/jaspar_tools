#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`

# Create a common directory for build output.

mkdir -p "$THISDIR/bin"

# Build each of the programs.

"$THISDIR/build_matrixaligner.sh"
"$THISDIR/build_pwmrandom.sh"
"$THISDIR/build_stamp.sh"

# vim: tabstop=4 expandtab shiftwidth=4
