#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

. "$THISDIR/tools/system.sh"

# Initialise directories for the installed programs.

BINDIR="$BASEDIR/bin"

# Set labels for SELinux, apply the labelling for SELinux.

if have semanage ; then
    semanage fcontext -a -t bin_t "$BINDIR(/.*)?"
    restorecon -R "$BINDIR"
fi

# vim: tabstop=4 expandtab shiftwidth=4
